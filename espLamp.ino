/*
Sketch for controlling a lamp (or any other switch) over WIFI with esp8266
Tested on devices: ESP-01 and OLIMEX ESP8266-EVB.


Set your wifi info and controll pin in the #defines below. Time zone may also be nice to set.

Features
- Device will get current time when booting from internet
- Web interface where you can configure time periods when the lamp is on/off.
- Switch can be turned on/off over wifi with <ip>/?set=t (toggle)

Bugs/TODO:
    <ip>/?set=on or <ip>/?set=off not implemented
...

Future improvments:
- Light on cont down timer, eg, set=h60 will kepp lights on for 60 minutes
- Android app to set the above
*/

#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <WiFiUdp.h>
#include <EEPROM.h>

// Define wifi in wifisecret.h or set #defines below
#include "wifisecret.h"
#ifndef WIFI_SSID
#define WIFI_SSID ssidname
#endif
#ifndef WIFI_PASS
#define WIFI_PASS password
#endif

//ALL TIMES IN MINUTES GMT
#ifndef TZ_MINUTES
#define TZ_MINUTES 60 /* Minutes offset from GMT */
#endif

#define SERIAL_DBG 0

// May want to swap HIGH and LOW if light is connected between pin and high
#define LIGHT_ON HIGH
#define LIGHT_OFF LOW
#define OLIMEX_EVB_RELAY 5
#define ESP_01_GPIO2 2
#define CONTROL_PIN OLIMEX_EVB_RELAY





unsigned long starttime = 0;
unsigned long min_last = 60;
bool server_started = false;
const char* ntpServerName = "1.se.pool.ntp.org";
const int NTP_PACKET_SIZE = 48; // NTP time stamp is in the first 48 bytes of the message
byte packetBuffer[ NTP_PACKET_SIZE]; //buffer to hold incoming and outgoing packets
int control_val;
bool firstLoop = true;
ESP8266WebServer server(80);

// Some function defs. Needed for some compilers
typedef struct {int on; int off;} Period;
void digitalWriteCtrlPin(int val);
unsigned long getTime();
void handleRoot();
void handlePeriod();
void handleNotFound();
void toggleCtrlPin();
void switchLight(int min_today, Period& p);
void readEepromPeriod(int addr, Period& p);

// HTML parameter names
#define SETPERIOD "setperiod"
#define ON "on"
#define OFF "off"

// Address
#define TIMEZONE_ADDR 0
#define PERIOD_A_ADDR 4
#define PERIOD_B_ADDR 8

Period periodA = {-1, -1};
Period periodB = {-1, -1};

void setup() 
{
  pinMode(CONTROL_PIN, OUTPUT);
  digitalWriteCtrlPin(LIGHT_OFF);
#if SERIAL_DBG
  Serial.begin(115200);
  delay(10);
  Serial.print("\nConnecting to ");
  Serial.print(WIFI_SSID);
#endif

  WiFi.begin(WIFI_SSID, WIFI_PASS);
  int timeout = 20;
  while (WiFi.status() != WL_CONNECTED && timeout--) {
    delay(500);
#if SERIAL_DBG
    Serial.print(".");
#endif
  }
#if SERIAL_DBG
  Serial.println(WiFi.localIP());
#endif
  EEPROM.begin(512);
  // TODO: READ time zone
  readEepromPeriod(PERIOD_A_ADDR, periodA);
  readEepromPeriod(PERIOD_B_ADDR, periodB);
}

void loop() 
{
  unsigned long ms = millis();
  unsigned long min_up = ms/(1000*60);
  unsigned long min_now = starttime + min_up;

  if (min_last  > min_now) {
#if SERIAL_DBG
    Serial.print("Get new time last:");
    Serial.print(min_last);
    Serial.print(" now:");
    Serial.println(min_now);
#endif
    starttime = getTime()/60;
    if (!starttime) {
      return;
    }
    starttime += TZ_MINUTES;
    min_now = starttime + min_up;
  }

  // If time recieved, start webserver
  if (starttime && !server_started) {
    server_started = true;
    server.on("/", handleRoot);
    server.on("/period", handlePeriod);
    server.onNotFound(handleNotFound);
    server.begin();
  }

  server.handleClient();

  if (min_last != min_now) {
    int min_today=int(min_now  % 1440L);
#if SERIAL_DBG
    Serial.print(min_today);
    Serial.print(", H:" );
    Serial.print(min_today / 60);
    Serial.print(" m:" );
//    Serial.println(min_now  % 60);
    Serial.print(min_now  % 60);
    Serial.print(" ft:" );
    Serial.println(firstLoop);
#endif
    switchLight(min_today, periodA);
    switchLight(min_today, periodB);
  }
  min_last = min_now;
  firstLoop = false;
  delay(1);
}

/*
 * Write value to digital pin and save value
 */
void digitalWriteCtrlPin(int val) {
  control_val = val;
  digitalWrite(CONTROL_PIN, control_val);
}

/*
 * Change value of control pin
 */
void toggleCtrlPin() {
  control_val = (control_val==HIGH?LOW:HIGH);
  digitalWrite(CONTROL_PIN, control_val);
}

/*
 * Switch lights when clock is exactly onTime or offTime
 * All times in minutes
 */
void switchLight(int min_today, Period& p) {
    if (p.on < 0 || p.off < 0) {
      return;
    }
#if SERIAL_DBG
      Serial.print("Handle first ");
      Serial.print(firstLoop);
      Serial.print(" p:");
      Serial.print(p.on);
      Serial.print(" ");
      Serial.print(p.off);
      Serial.print(" mt:");
      Serial.println(min_today);
#endif
    if (firstLoop && p.on < min_today && min_today < p.off) {
      digitalWriteCtrlPin(LIGHT_ON);
      return;
    }
    //Transition change
    if (min_today == p.on) {
      digitalWriteCtrlPin(LIGHT_ON);
    }
    if (min_today == p.off) {
      digitalWriteCtrlPin(LIGHT_OFF);
    }
}


// send an NTP request to the time server at the given address
unsigned long sendNTPpacket(IPAddress& address, WiFiUDP& udp)
{
#if SERIAL_DBG
  Serial.println("sending NTP packet...");
#endif
// set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12]  = 49;
  packetBuffer[13]  = 0x4E;
  packetBuffer[14]  = 49;
  packetBuffer[15]  = 52;

  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:
  udp.beginPacket(address, 123); //NTP requests are to port 123
  udp.write(packetBuffer, NTP_PACKET_SIZE);
  udp.endPacket();
}

/* Returns seconds since 1970*/
unsigned long getTime() {
  WiFiUDP udp;
  unsigned int localPort = 2390;      // local port to listen for UDP packets  
  IPAddress timeServerIP; // time.nist.gov NTP server address

  udp.begin(localPort);
  WiFi.hostByName(ntpServerName, timeServerIP); 
  sendNTPpacket(timeServerIP, udp); // send an NTP packet to a time server

  delay(500);
  int cb = udp.parsePacket();
  if (!cb) {
#if SERIAL_DBG
    Serial.println("no packet yet");
#endif
    return 0;
  } else {
    // We've received a packet, read the data from it
    udp.read(packetBuffer, NTP_PACKET_SIZE); // read the packet into the buffer
    unsigned long highWord = word(packetBuffer[40], packetBuffer[41]);
    unsigned long lowWord = word(packetBuffer[42], packetBuffer[43]);
    unsigned long secsSince1900 = highWord << 16 | lowWord;
    // now convert NTP time into everyday time:
#if SERIAL_DBG
    Serial.print("Unix time = ");
#endif
    // Unix time starts on Jan 1 1970. In seconds, that's 2208988800:
    const unsigned long seventyYears = 2208988800UL;
    // subtract seventy years:
    unsigned long epoch = secsSince1900 - seventyYears;
    // print Unix time:
#if SERIAL_DBG
    Serial.println(epoch);

    Serial.print("H: " );
    Serial.print((epoch  % 86400L) / 3600); // print the hour (86400 equals secs per day)
    Serial.print("M: " );
    Serial.print((epoch  % 3600) / 60); // print the minute (3600 equals secs per minute)
#endif
    return epoch;
  }
}

#define MY_BODY_SIZE 800

void getPeriodString(const char *label, Period& p, char *buff, int len) {
  if (p.on < 0 || p.off < 0) {
    snprintf(buff, len, "%s not set", label);
  } else {
    snprintf(buff, len, "%s %02d:%02d - %02d:%02d", label, p.on/60,p.on%60, p.off/60,p.off%60);
  }
}

void saveTime(int addr, String time) {
  long val = time.toInt();
  byte bval = (byte)val;
  EEPROM.write(addr, bval);
  addr++;
  int ix = time.indexOf(':');
  val = 0;
  if (ix>=0) {
    val = time.substring(ix+1).toInt();
  }
  bval = (byte)val;
  EEPROM.write(addr, bval);
}

void handleRoot() {
  char temp[MY_BODY_SIZE];
  char pAStr[32];
  char pBStr[32];
  unsigned long ms = millis();
  unsigned long min_up = ms/(1000*60);
  unsigned long now = starttime + min_up;
  int min_today=int(now  % 1440L);

  if (server.arg("set") == "t") {
      toggleCtrlPin();
  } else if (server.hasArg(SETPERIOD)) {
    int addr = -1;
    if (server.arg(SETPERIOD) == "a") {
      addr = PERIOD_A_ADDR;
    } else if (server.arg(SETPERIOD) == "b") {
      addr = PERIOD_B_ADDR;
    }
    if (addr > 0) {
#if SERIAL_DBG
      Serial.print("Set ");
      Serial.print(server.arg(SETPERIOD));
      Serial.print(" disable: ");
      Serial.println(server.arg("disable"));
      Serial.print(server.arg(ON));
      Serial.println(server.arg(OFF));
#endif
      if (server.hasArg("disable")) {
        EEPROM.write(addr, 255);
        EEPROM.write(addr+1, 255);
        EEPROM.write(addr+2, 255);
        EEPROM.write(addr+3, 255);
      } else {
        saveTime(addr, server.arg(ON));
        saveTime(addr+2, server.arg(OFF));
      }
      EEPROM.commit();
      // Re read values
      if (addr == PERIOD_A_ADDR) {
        readEepromPeriod(addr, periodA);
      } else {
        readEepromPeriod(addr, periodB);
      }
    }
    // TODO: Create and destroy eeprom object on every write
  }

  getPeriodString("A", periodA, pAStr, 32);
  getPeriodString("B", periodB, pBStr, 32);

  snprintf(temp, MY_BODY_SIZE,
  "<html><head><title>Switcheroo</title><meta name='viewport' content='width=device-width, initial-scale=1'>\
  <style>body {background-color: #cccccc; font-family: Arial, Helvetica, Sans-Serif; Color: #000088;}</style></head>\
  <body><h1>Switcheroo</h1><p>Time: %02d:%02d</p>\
  <form action='/'><input type='hidden' name='set' value='t'><button type='submit'>Switch lights</button></form>\
  Lights on:<br/>%s<br/>%s</p><a href='period?p=a'>Set period A</a> &nbsp; <a href='period?p=b'>Set period B</a>\
  </body></html>",
  min_today / 60 , min_today % 60, pAStr, pBStr, server.arg("set").c_str());
  server.send ( 200, "text/html", temp );
}

void handlePeriod() {
  char temp[MY_BODY_SIZE];
  unsigned long ms = millis();
  unsigned long min_up = ms/(1000*60);
  unsigned long now = starttime + min_up;
  int min_today=int(now  % 1440L);
  String period=server.arg("p");

  int startP = 0;
  int stopP = 0;
  if (period=="a") {
    startP = periodA.on;
    stopP = periodA.off;
  } else {
    startP = periodB.on;
    stopP = periodB.off;
  }

  snprintf(temp, MY_BODY_SIZE,
  "<html><head><title>Set period</title><meta name='viewport' content='width=device-width, initial-scale=1'>\
  <style>body { background-color: #cccccc; font-family: Arial, Helvetica, Sans-Serif; Color: #000088; }</style></head>\
  <body><h1>Switcheroo</h1><p>Time: %02d:%02d</p>\
  <h3>Set lighs on period %s<h3/>\
  <form action='/' method='POST'>On <input type='time' name='on' value='%02d:%02d'><br/>\
  Off <input type='time' name='off' value='%02d:%02d'><br>\
  <input type='hidden' name='setperiod' value='%s'><br/><button type='submit'>Set period</button>\
  <button type='submit' name='disable' >Disable period</button></form>\
  <a href='/'>Home</a></body></html>",
  min_today / 60 , min_today % 60, period.c_str(), startP/60, startP%60, stopP/60, stopP%60, period.c_str());

  server.send ( 200, "text/html", temp );
}

void handleNotFound() {
  server.send ( 404, "text/plain", "Oh nose!");
}

int eepromReadHM(int addr) {
  long h;
  long m;
  h = EEPROM.read(addr);
  if (h<0 || h >=24) {
    return -1;
  }
  addr++;
  m = EEPROM.read(addr);
  if (m < 0 && m >= 60) {
    return -1;
  }
  return (int)(h*60+m);
}

void readEepromPeriod(int addr, Period& p) {
  p.on = eepromReadHM(addr);
  p.off = eepromReadHM(addr+2);
#if SERIAL_DBG
  Serial.print("Read eeprom:");
  Serial.print(p.on/60);
  Serial.print(":");
  Serial.print(p.on%60);
  Serial.print("-");
  Serial.print(p.off/60);
  Serial.print(":");
  Serial.println(p.off%60);
#endif
}

