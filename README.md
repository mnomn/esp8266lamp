# README #

Sketch for controlling a lamp (or any other switch) over WIFI with esp8266.
Tested on devices: ESP-01 and OLIMEX ESP8266-EVB.

Set your preferences in the #defines in source code file, such as wifi settings, timezone.

Most ESP 8266 chips are hard to work with (boot modes/3.3v/strange pin config, etc). The OLIMEX ESP8266-EVB is reasonably easy to use and has a built in relay.

### Features ###
- Device will get current time when booting from internet (NTP)
- Web interface to set intervall where the lamp is on/off.
- Lamp can be turned on/off over wifi <ip>/?set=h or <ip>/?set=l